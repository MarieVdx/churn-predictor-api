# Churn Predictor 

Contributors: Antoine Meilliez, Marie Verdoux, Rayann Harmouni

## Usage
### 1 - Installation
You need an updated version of Python 3.8 to run this project. Check it with 
```
$ python3 --version
```
Process the following steps to install the project
```
# Clone the repository
$ git clone https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-2.git

# Move in the repository
$ cd chaos-2

# Initialize the project
$ source init.sh
$ source activate.sh

# Your Are Ready !
```
## 2 - Run the API locally

In your shell, run the following command:
``` 
$ python chaos/application/server.py

```
You should see an hypertext link: http://0.0.0.0:5000/



If you click over the link, you can see the different commands you could do with the API. 

If you want to run command API commands, open another shell thumbnail and place yourself on the chaos-2 repository. 

``` 
$ cd chaos/application

```

 - Create customer :
 
```
$ curl -X POST "http://0.0.0.0:5000/customers/" -H  "accept: application/json" -H  "Content-Type: application/json" -d @client.json

or 

$ curl -X POST "http://0.0.0.0:5000/customers/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"ID_CLIENT\": id,  \"NOM\": \"character\",  \"DATE_ENTREE\": \"date\",  \"PAYS\": \"character\",  \"SEXE\": \"character\",  \"MEMBRE_ACTIF\": \"booleen\",  \"NB_PRODUITS\": nombre,  \"CARTE_CREDIT\": \"booleen\",  \"AGE\": integer,  \"BALANCE\": float,  \"SCORE_CREDIT\": float,  \"SALAIRE\": float}"
```
 
 - Delete a customer given its identifier:
 
```
$ curl -X DELETE "http://0.0.0.0:5000/customers/id" -H  "accept:  
 application/json"
 
```
 - Fetch a given customer
 
``` 
$ curl -X GET "http://0.0.0.0:5000/customers/5678" -H  "accept: application/json"

```

 - Update customer
 
```
$ curl -X PUT "http://0.0.0.0:5000/customers/id" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"ID_CLIENT\": id,  \"NOM\": \"character\",  \"DATE_ENTREE\": \"date\",  \"PAYS\": \"character\",  \"SEXE\": \"character\",  \"MEMBRE_ACTIF\": \"booleen\",  \"NB_PRODUITS\": nombre,  \"CARTE_CREDIT\": \"booleen\",  \"AGE\": integer,  \"BALANCE\": float,  \"SCORE_CREDIT\": float,  \"SALAIRE\": float}"

```

 -  Make a prediction: 
 
``` 
$ curl -X  curl -X POST "http://0.0.0.0:5000/predictions/churn" -H  "accept: application/json" -H  "Content-Type: application/json" -d @client.json

or 

$  curl -X  curl -X POST "http://0.0.0.0:5000/predictions/churn" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"ID_CLIENT\": id,  \"NOM\": \"character\",  \"DATE_ENTREE\": \"date\",  \"PAYS\": \"character\",  \"SEXE\": \"character\",  \"MEMBRE_ACTIF\": \"booleen\",  \"NB_PRODUITS\": nombre,  \"CARTE_CREDIT\": \"booleen\",  \"AGE\": integer,  \"BALANCE\": float,  \"SCORE_CREDIT\": float,  \"SALAIRE\": float}"
```
## 3 - Run the API online

## 4 - Documentation

To see the documentation :

```
$ cd docs

$ firefox _build/html/index.html
or 
$ google-chrome _build/html/index.html

```

## Project Organisation

``` 

├── activate.sh
├── chaos
│   ├── application
│   │   ├── false_client.json
│   │   ├── __init__.py
│   │   ├── __pycache__
│   │   │   ├── __init__.cpython-38.pyc
│   │   │   ├── server.cpython-38.pyc
│   │   │   └── train_model_for_appetence.cpython-38.pyc
│   │   └── server.py
│   ├── domain
│   │   ├── constraints.py
│   │   ├── customer.py
│   │   ├── __init__.py
│   │   └── __pycache__
│   │       ├── customer.cpython-38.pyc
│   │       ├── __init__.cpython-38.pyc
│   │       └── messages.cpython-38.pyc
│   ├── infrastructure
│   │   ├── config
│   │   │   ├── config.py
│   │   │   ├── config_template.yml
│   │   │   ├── config.yml
│   │   │   ├── key.json
│   │   │   └── __pycache__
│   │   │       └── config.cpython-38.pyc
│   │   ├── connection.py
│   │   ├── connexion_functions.py
│   │   ├── customers_table.py
│   │   ├── __init__.py
│   │   ├── model.py
│   │   └── __pycache__
│   │       ├── connection.cpython-38.pyc
│   │       ├── connexion.cpython-38.pyc
│   │       ├── connexion_functions.cpython-38.pyc
│   │       ├── customers_table.cpython-38.pyc
│   │       ├── __init__.cpython-38.pyc
│   │       └── model.cpython-38.pyc
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-38.pyc
│   │   └── settings.cpython-38.pyc
│   ├── settings.py
│   └── test
│       ├── functional
│       │   ├── client.json
│       │   ├── __init__.py
│       │   ├── __pycache__
│       │   │   ├── __init__.cpython-38.pyc
│       │   │   ├── test_app.cpython-38.pyc
│       │   │   ├── test_customer.cpython-38.pyc
│       │   │   ├── test_server.cpython-38.pyc
│       │   │   └── test_server.cpython-38-pytest-5.4.2.pyc
│       │   ├── test_app.py
│       │   ├── test_customer.py
│       │   └── test_server.py
│       ├── __init__.py
│       ├── __pycache__
│       │   └── __init__.cpython-38.pyc
│       └── unit
│           ├── __init__.py
│           └── __pycache__
│               ├── __init__.cpython-38.pyc
│               ├── test.cpython-38.pyc
│               ├── test_customer.cpython-38.pyc
│               └── test_hello_world.cpython-38-pytest-5.4.2.pyc
├── cloud_sql_proxy
├── conf.py
├── deployment
│   └── deployment_template.yml
├── Dockerfile
├── docs
│   ├── _build
│   │   ├── doctrees
│   │   │   ├── application.doctree
│   │   │   ├── chaos-2.chaos.application.doctree
│   │   │   ├── chaos-2.chaos.doctree
│   │   │   ├── chaos-2.chaos.domain.doctree
│   │   │   ├── chaos-2.chaos.infrastructure.doctree
│   │   │   ├── chaos-2.chaos.test.doctree
│   │   │   ├── chaos-2.doctree
│   │   │   ├── chaos.application.doctree
│   │   │   ├── chaos.doctree
│   │   │   ├── chaos.domain.doctree
│   │   │   ├── chaos.infrastructure.doctree
│   │   │   ├── chaos.test.doctree
│   │   │   ├── chaos.test.functional.doctree
│   │   │   ├── chaos.test.functional.test_app.doctree
│   │   │   ├── chaos.test.functional.test_customer.doctree
│   │   │   ├── chaos.test.unit.doctree
│   │   │   ├── chaos.test.unit.test_server.doctree
│   │   │   ├── environment.pickle
│   │   │   ├── index.doctree
│   │   │   └── modules.doctree
│   │   └── html
│   │       ├── application.html
│   │       ├── chaos-2.chaos.application.html
│   │       ├── chaos-2.chaos.domain.html
│   │       ├── chaos-2.chaos.html
│   │       ├── chaos-2.chaos.infrastructure.html
│   │       ├── chaos-2.chaos.test.html
│   │       ├── chaos-2.html
│   │       ├── chaos.application.html
│   │       ├── chaos.domain.html
│   │       ├── chaos.html
│   │       ├── chaos.infrastructure.html
│   │       ├── chaos.test.functional.html
│   │       ├── chaos.test.functional.test_app.html
│   │       ├── chaos.test.functional.test_customer.html
│   │       ├── chaos.test.html
│   │       ├── chaos.test.unit.html
│   │       ├── chaos.test.unit.test_server.html
│   │       ├── genindex.html
│   │       ├── index.html
│   │       ├── modules.html
│   │       ├── objects.inv
│   │       ├── py-modindex.html
│   │       ├── search.html
│   │       ├── searchindex.js
│   │       ├── _sources
│   │       │   ├── application.rst.txt
│   │       │   ├── chaos-2.chaos.application.rst.txt
│   │       │   ├── chaos-2.chaos.domain.rst.txt
│   │       │   ├── chaos-2.chaos.infrastructure.rst.txt
│   │       │   ├── chaos-2.chaos.rst.txt
│   │       │   ├── chaos-2.chaos.test.rst.txt
│   │       │   ├── chaos-2.rst.txt
│   │       │   ├── chaos.application.rst.txt
│   │       │   ├── chaos.domain.rst.txt
│   │       │   ├── chaos.infrastructure.rst.txt
│   │       │   ├── chaos.rst.txt
│   │       │   ├── chaos.test.functional.rst.txt
│   │       │   ├── chaos.test.functional.test_app.rst.txt
│   │       │   ├── chaos.test.functional.test_customer.rst.txt
│   │       │   ├── chaos.test.rst.txt
│   │       │   ├── chaos.test.unit.rst.txt
│   │       │   ├── chaos.test.unit.test_server.rst.txt
│   │       │   ├── index.rst.txt
│   │       │   └── modules.rst.txt
│   │       └── _static
│   │           ├── alabaster.css
│   │           ├── basic.css
│   │           ├── custom.css
│   │           ├── doctools.js
│   │           ├── documentation_options.js
│   │           ├── file.png
│   │           ├── jquery-3.5.1.js
│   │           ├── jquery.js
│   │           ├── language_data.js
│   │           ├── minus.png
│   │           ├── plus.png
│   │           ├── pygments.css
│   │           ├── searchtools.js
│   │           ├── underscore-1.3.1.js
│   │           └── underscore.js
│   ├── chaos.application.rst
│   ├── chaos.domain.rst
│   ├── chaos.infrastructure.rst
│   ├── chaos.rst
│   ├── chaos.test.functional.rst
│   ├── chaos.test.rst
│   ├── chaos.test.unit.rst
│   ├── conf.py
│   ├── index.rst
│   ├── make.bat
│   ├── Makefile
│   ├── modules.rst
│   ├── requirements.txt
│   ├── _static
│   └── _templates
├── index.rst
├── __init__.py
├── init_sql.sh
├── make.bat
├── Makefile
├── new_customer.json
├── README.md
├── requirements.txt
├── run_sql_proxy.sh
├── setup.py
└── source
    └── conf.py


```